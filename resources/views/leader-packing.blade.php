<html>
<head>
    <title>Leader Packing</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <!-- <link rel="stylesheet" href="{{url('css/generate-indent.css')}}"> -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />
    <style media="screen">
      #main{
        margin-top: 60px;
      }
      form{
        margin: 20px 0px;
      }
      select,input[type="text"]{
        width: 100%;
      }
      table{
        margin-top: 30px;
        font-size: 12px;
      }
      tbody{
        text-align: center;
      }
      .p-lr-0{
        padding: 0px;
      }

      input[name='scale_weight']{
        background-color: #6da0ec;
        font-size: 30px;
        text-align: center;
        font-weight: bolder;
      }
      .material-icons{
        cursor: pointer;
      }
      input[type="submit"]{
        margin-top: 20px;
      }
    </style>
</head>
<body>
  <div id="loader" class="loader"></div>
  <section id="header">
      <header>
          <nav class="navbar navbar-default navbar-fixed-top">
              <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                              data-target="#navbar-collapse" aria-expanded="false">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="navbar-collapse">
                      <ul class="nav navbar-nav">
                          <li><a href="/home">Packing</a></li>
                          <li><a href="/ncr-packing">NCR Packing</a></li>
                          <li class="active"><a class="active-menu" href="/leader-packing">Leader Packing</a></li>
                          <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                              data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Braiding <i class="material-icons">
                                  arrow_drop_down
                              </i></a>
                              <ul class="dropdown-menu">
                                  <li><a class="" href="/braiding">Regular</a></li>
                                  <li><a class="" href="#">NCR</a></li>
                              </ul>
                          </li>
                          <li class="dropdown">
                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                  data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Others <i class="material-icons">
                                      arrow_drop_down
                                  </i></a>
                                  <ul class="dropdown-menu">
                                      <li><a href="/get-packed-cases">Packed Cases</a></li>
                                      <li><a href="/print-sticker-range">Manual Sticker</a></li>
                                      <li><a href="/re-packing">Re-Packing</a></li>

                                  </ul>
                              </li>
                      </ul>
                  </div>
              </div>
          </nav>
      </header>

</section>

<section id="main">
    <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 col-lg-6 col-sm-12">
            <h3>Leader/SLB/WASTE Packing</h3>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="col-md-12" action="/add-leader-indirect-packing" method="post">
              @csrf
              <input type="hidden" name="master_id" value="0">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="box-type">Box Type</label>
                      <select name="box-type" id="box-type" required>


                        <option value="C" data-pack="C" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">C 21"X21"X12.5"</option>
                        <option value="CS" data-pack="CS" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">CS 21"X21"X12.5"</option>
                        <option value="CIS" data-pack="CIS" data-box="CORRUGATED BOX 2" data-weight="1.6" data-count="{{$count->cCount}}">CIS 22"X21"X13.5"</option>
                        <option value="CKS" data-pack="CKS" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">CKS 21"X21"X12.5"</option>

                        <option value="B" data-pack="B" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">B 24"X12"X13"</option>
                        <option value="B" data-pack="B" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">B 24"X12"X14"</option>
                        <option value="B" data-pack="B" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">B 25"X12.5"X15"</option>

                        <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">BS 24"X12"X13"</option>
                        <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">BS 24"X12"X14"</option>
                        <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">BS 25"X12.5"X15"</option>

                        <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">BIS 24"X12"X13"</option>
                        <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">BIS 24"X12"X14"</option>
                        <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">BIS 25"X12.5"X15"</option>

                        <option value="BYIS" data-pack="BYIS" data-box="CORRUGATED BOX 2" data-weight="1.6" data-count="{{$count->byCount}}">BYIS 22"X21"X13.5"</option>
                        <option value="BYIS" data-pack="BYIS" data-box="CORRUGATED BOX 3" data-weight="1.4" data-count="{{$count->byCount}}">BYIS 17.5"X15.5"X18.5"</option>


                        <option value="BYS" data-pack="BYS" data-box="CORRUGATED BOX 4" data-weight="1.6" data-count="{{$count->byCount}}">BYS 20.5"X20.5"X13.5"</option>

                        <option value="HDPE" data-pack="HDPE" data-box="-" data-weight="0.1" data-count="{{$count->hCount}}">HDPE</option>


                        {{-- <option value="B" data-count="{{$count->bCount}}">B</option>
                        <option value="BI" data-count="{{$count->bCount}}">BI</option>
                        <option value="BIS" data-count="{{$count->bCount}}">BIS</option>
                        <option value="BS" data-count="{{$count->bCount}}">BS</option>

                        <option value="C" data-count="{{$count->cCount}}">C</option>
                        <option value="CI" data-count="{{$count->cCount}}">CI</option>
                        <option value="CIS" data-count="{{$count->cCount}}">CIS</option>
                        <option value="CS" data-count="{{$count->cCount}}">CS</option>
                        <option value="CK" data-count="{{$count->cCount}}">CK</option>
                        <option value="CKS" data-count="{{$count->cCount}}">CKS</option>

                        <option value="BYS" data-count="{{$count->byCount}}">BYS</option>
                        <option value="BYIS" data-count="{{$count->byCount}}">BYIS</option> --}}

                      </select>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="case-no">Case No</label>
                      <input type="text" id="case-no" name="case-no" class="text-input">
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="info">Info</label>
                      <input type="text" id="info" name="info" class="text-input">
                    </div>
                  </div>
                  <input type="hidden" name="packing-box" id="packing-box">
                  {{-- <div class="col-md-4">
                    <div class="form-group">
                      <label for="packing-box">Packing Box</label>
                      <select id="packing-box" name="packing-box" class="text-input">
                        @foreach($packingResult as $name=>$packing)
                        <option value="{{$name}}" data-weight="{{$packing['value']}}">{{$name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div> --}}

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="box-weight">Box Weight</label>
                      <input type="text" id="box-weight" name="box-weight" class="text-input" value="1.6">
                  </div>
                  </div>

                  <div class="col-md-4">
                  <div class="form-group">
                    <label for="package-type">Package Type</label>
                    <select id="package-type" name="package-type" class="text-input" required>
                      <option value="-">NONE</option>
                      <option value="BOBBIN">BOBBIN</option>
                      <option value="CHEESE">CHEESE</option>
                      <option value="BRAIDINGYARN">BRAIDING YARN</option>
                    </select>
                  </div>
                  </div>

                  <div class="col-md-4">
                  <div class="form-group">
                    <label for="package-date">Package Date</label>
                    <input type="text" id="package-date" name="package-date" class="text-input" required>
                  </div>
                  </div>

                  <div class="col-md-4">
                  <div class="form-group">
                    <label for="bobbin-count">Bobbin Count</label>
                    <input type="text" id="bobbin-count" name="bobbin-count" class="text-input" required value="0">
                  </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                        <label for="tare-weight">Tare Weight</label>
                        <input type="text" name="tare_weight" id="tare-weight" class="text-input" value="" required>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="material">Material</label>
                      <select class="text-input" name="material" id="material" required>
                        <?php foreach ($material as $key => $value): ?>
                          <option value="{{$value->id}}">{{$value->descriptive_name}}</option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>


                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="material_type">Material Type</label>
                      <select class="text-input" name="material_type" id="material_type" required>
                        <option value="leader">Leader</option>
                        <option value="SLB">SLB</option>
                        <option value="WASTE">WASTE</option>
                      </select>
                    </div>
                  </div>


                  <div class="col-md-8">
                    <div class="form-group">
                      <label for="scale">Scale Weight</label>
                      <input type="text" name="scale_weight" class="text-input" id="g_wt" value="">
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="from-group">
                      <input type="submit" class="btn btn-primary" value="Submit">
                    </div>
                  </div>
            </form>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-12 p-lr-0" style="max-height: 80vh;overflow: auto;">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>Case No</th>
                  <th>Material</th>
                  <th>Material Type</th>
                  <th>No Of Spindle</th>
                  <th>Scale Weight</th>
                  <th>Actual Weight</th>
                  <th>Print QR</th>
                  <!-- <th>Edit</th> -->
                  <!-- <th>Delete</th> -->
                </tr>
              </thead>
              <tbody>
                <?php foreach ($packedEntry as $key => $value): ?>
                  <tr>
                    <td>{{$value->case_no}}</td>
                    <td>{{$value->item_master->material}}</td>
                    <td>{{$value->material_type}}</td>
                    <td>{{$value->bobbin_count}}</td>
                    <?php $scale=0; $actual=0; foreach ($value->weight_logs as $log): ?>
                      <?php
                          $scale += $log->total_weight;
                          $actual += $log->material_weight;
                      ?>
                    <?php endforeach; ?>
                    <td>{{$scale}}</td>
                    <td>{{$actual}}</td>
                    <td><i class="material-icons" data-id="{{$value->id}}">print</i></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>



</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('input[type="text"]').attr("autocomplete", "off");

    $('#box-type').on('change', function(){
        $('#case-no').val($(this).find(':selected').data('count'));
        $('#box-weight').val($(this).find(':selected').data('weight'));
        $('#packing-box').val($(this).find(':selected').data('box'));
    });

    var socket = io('http://127.0.0.1:3000');

    socket.on('laravel_database_weightlog-channel:weightLogEvent', function(data){
        $('#g_wt').val(data);
    });

    var lastEntry = {!! json_encode($packedEntry[0]) !!}

    if(lastEntry != null){

      $('#box-type').val(lastEntry.box_type).change();
      $('#package-type').val(lastEntry.package_type).change();
      $('#package-date').val(lastEntry.packed_date);
      $('#packing-box').val(lastEntry.packing_box);
      $('#moisture-regain').val(lastEntry.moisture_weight);
      $('#material').val(lastEntry.material_id);
      $('#case-no').val(parseInt(lastEntry.case_no)+1);
      $('#info').val(lastEntry.reason);
      $('#tare-weight').val(lastEntry.weight_logs[0].tare_weight);
      $('#bobbin-count').val(lastEntry.bobbin_count);
      $('#material_type').val(lastEntry.material_type);
      $('#box-weight').val(lastEntry.box_weight);
    }

  });

</script>

</body>
</html>
