<html>
<head>
    <title>Packaging</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>
        #main{
            margin-top: 100px;
        }

        .wl-card{
            width: 100%;
            height: 400px;
            /*box-shadow: 0px 0px 5px rgba(0,0,0, 0.2);*/
            /*margin-top: 10%;*/
        }


        .text-danger{
            color: #ff1744;
            margin-top: 10px;
            display: inline-block;
        }

        #ui-datepicker-div{
            width: 250px;
        }

        th{
            text-align: center;
        }

        tr>td:nth-child(3){
            width: 4%;
        }

        tr>td:nth-child(2){
            width: 10%;
        }

        tr>td:nth-child(4){
            width: 8%;
        }

        tr>td:nth-child(1){
            width: 10%;
        }

        tr>td{
            text-align: center;
        }

        .wl-card>h3{
            margin-top: 20px;
            margin-bottom: 20px;

        }

        select:disabled{
            background-color: gainsboro !important;
        }

        .form-group{
            margin-bottom: 20px;
        }
        .add-doff{
            width: 80px;
            background: none;
            border: none;
            margin-left: 25px;
            margin-bottom: 20px;
            font-weight: 600;
            color: blue;
        }

        h2{
            margin-top: 0px !important;
        }
        form{
            margin-top: 10px !important;
        }
        #g_wt{
            background: #6da0ec;
            text-align: center;
            font-size: 20px;
            font-weight: 800;
        }
    </style>

    <div id="loader" class="loader"></div>
    <section id="header">
        <header>
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a class="active-menu" href="/home">Packing</a></li>
                            <li><a href="/ncr-packing">NCR Packing</a></li>
                            <li><a href="/leader-packing">Leader Packing</a></li>
                            <li class="dropdown active">
                                <a href="#" class="dropdown-toggle active-menu" data-toggle="dropdown" role="button"
                                data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Braiding <i class="material-icons">
                                    arrow_drop_down
                                </i></a>
                                <ul class="dropdown-menu">
                                    <li><a class="active-menu" href="/braiding">Regular</a></li>
                                    <li><a class="" href="#">NCR</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Others <i class="material-icons">
                                        arrow_drop_down
                                    </i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/get-packed-cases">Packed Cases</a></li>
                                        <li><a href="/print-sticker-range">Manual Sticker</a></li>
                                        <li><a href="/re-packing">Re-Packing</a></li>

                                    </ul>
                                </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

    </section>
</head>
<body>

<section id="main">
    <div class="container-fluid">
        <h2>Braiding Packing</h2>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
            <div class="row">
                <form action="/create-braiding" method="POST" class="col-lg-12 col-md-12 col-sm-12">
                    @csrf
                    <input type="hidden" name="packing_box" id="packing_box" value="">
                <div class="col-lg-12 col-md-12 col-sm-12 doff-unit">
                    <div class="col-md-3">
                        <input type="hidden" name="new_material[]" id="material-0">
                        <div class="form-group">
                            <label for="doff_no">Doff No</label>
                            <select name="doff_no[]" class="doff_no" data-val="0" required>
                                <option value="" selected disabled>Select Doff</option>
                                @foreach ($count->doffs as $doff=>$value)
                                    <option value="{{$doff}}">{{$doff}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="material_id">Material</label>
                            <select name="material_id[]" id="material_id-0" class="material_id" data-val="0" required>
                                <option value="">Material</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="filament">Filament Type:</label>
                            <select name="filament[]" id="filament-0" required>
  
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="b_count">Bobbin Count  <span id="count-0"></span></label>
                            <input type="text" class="text-input number required" class="b_count" name='b_count[]' required>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <input class="add-doff" type="button" value="Add Doff">
                </div>
    
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="box-type">Box and Packing Type</label>
                            <select name="box-type" id="box-type" required>
    
                                <option value="C" data-pack="C" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">C 21"X21"X12.5"</option>
                                <option value="CS" data-pack="CS" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">CS 21"X21"X12.5"</option>
                                <option value="CIS" data-pack="CIS" data-box="CORRUGATED BOX 2" data-weight="1.6" data-count="{{$count->cCount}}">CIS 22"X21"X13.5"</option>
                                <option value="CKS" data-pack="CKS" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">CKS 21"X21"X12.5"</option>
    
                                <option value="B" data-pack="B" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">B 24"X12"X13"</option>
                                <option value="B" data-pack="B" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">B 24"X12"X14"</option>
                                <option value="B" data-pack="B" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">B 25"X12.5"X15"</option>
    
                                <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">BS 24"X12"X13"</option>
                                <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">BS 24"X12"X14"</option>
                                <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">BS 25"X12.5"X15"</option>
    
                                <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">BIS 24"X12"X13"</option>
                                <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">BIS 24"X12"X14"</option>
                                <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">BIS 25"X12.5"X15"</option>
    
                                <option value="BYIS" data-pack="BYIS" data-box="CORRUGATED BOX 2" data-weight="1.6" data-count="{{$count->byCount}}">BYIS 22"X21"X13.5"</option>
                                <option value="BYIS" data-pack="BYIS" data-box="CORRUGATED BOX 3" data-weight="1.4" data-count="{{$count->byCount}}">BYIS 17.5"X15.5"X18.5"</option>
    
    
                                <option value="BYS" data-pack="BYS" data-box="CORRUGATED BOX 4" data-weight="1.6" data-count="{{$count->byCount}}">BYS 20.5"X20.5"X13.5"</option>
    
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="case-no">Case No</label>
                            <input type="text" id="case-no" name="case-no" class="text-input" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="info">Remarks</label>
                            <input type="text" id="info" name="info" class="text-input">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="moisture-regain">MR <a class="change-moisture" style="font-size: 14px; float: right; color: #003EBB;" >Change</a></label>
                            <input type="text" id="moisture-regain" name="moisture-regain" class="text-input required" required>
                        </div>
                    </div>
                </div>
    
                <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="box-weight">Box Weight</label>
                            <input type="text" id="box-weight" name="box-weight" class="text-input required" value="1.6" required>
                        </div>
                    </div>
    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="package-date">Package Date | DOM : <span id="dom">{{$manufacturedDate}}</span></label>
                            <input type="text" id="package-date" name="package-date" class="text-input required" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="inset-weight">Inset Weight</label>
                            <select name="inset-weight" id="inset-weight" class="text-input" required>
                                <option value="0">0</option>
                                <option value="0.1">0.10</option>
                                <option value="0.2">0.20</option>
                                <option value="0.3">0.30</option>
                                <option value="0.4">0.40</option>
                                <option value="0.5">0.50</option>
                                <option value="0.6">0.60</option>
                                <option value="0.7">0.70</option>
                                <option value="0.8">0.80</option>
                                <option value="0.9">0.90</option>
                                <option value="1">1</option>
    
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="dispatch-date">Dispatch Date</label>
                            <select id="dispatch-date" name="dispatch-date" class="text-input" required>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
    
                </div>
    
                <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="expiry-time">Expiry Time</label>
                            <select type="text" id="expiry-time" name="expiry-time" class="text-input">
                                <option value="" disabled selected>--</option>
                                <option value="3">3 Months</option>
                                <option value="6">6 Months</option>
                                <option value="9">9 Months</option>
                                <option value="12">1 Year</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="expiry-date">Expiry Date</label>
                            <input type="text" id="expiry-date" name="expiry-date" class="text-input"> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="package-type">Package Type</label>
                            <select id="package-type" name="package-type" class="text-input" required>
                                <option value="BOBBIN">BOBBIN</option>
                                <option value="CHEESE">CHEESE</option>
                                <option value="BRAIDINGYARN">BRAIDING YARN</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="g_wt">Gross Weight</label>
                            <input type="text" class="text-input decimal" id="g_wt" name="g_wt" required>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <input type="submit" class="btn btn-primary center-block" id="create-package" style="width: 10%;" value="Create Package">
                </div>
            </form>
        </div>
    </div>

    
</section>

    <div id="responsePopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Inspection Update Status</h4>
                </div>
                <div class="modal-body">
                    <h4 id="response"  style="margin-bottom: 20px;"></h4>
                    <input type="button" class="btn btn-primary center-block" style="width: 100px" onClick="window.location.reload()" value="Done">
                </div>
            </div>

        </div>
    </div>


    <div id="moisturePopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Supervisor MR Change.</h4>
                </div>
                <div class="modal-body">
                    <form action="#" name="set-moisture-form" id="set-moisture-form">
                        <div class="form-group">
                            <label for="moisture">M Value</label>
                            <input type="number" name="moisture" class="text-input" id="moisture" step="0.1">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="text-input" id="password">
                        </div>

                        <div class="form-group">
                            <input type="submit" name="password" class="btn btn-primary" value="Change Moisture">
                        </div>
                        <p id="moisture-response" class="text-danger" style="padding: 5px; display: none;"></p>
                    </form>
                </div>
            </div>

        </div>
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>


<script>


    $(document).ready(function(){
        var doff_count = 1;
        var doffs = {!! json_encode($count->doffs) !!};

        Date.isLeapYear = function (year) {
            return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
        };

        Date.getDaysInMonth = function (year, month) {
            return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
        };

        Date.prototype.isLeapYear = function () {
            return Date.isLeapYear(this.getFullYear());
        };

        Date.prototype.getDaysInMonth = function () {
            return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
        };

        Date.prototype.addMonths = function (value) {
            var n = this.getDate();
            this.setDate(1);
            this.setMonth(this.getMonth() + value);
            this.setDate(Math.min(n, this.getDaysInMonth()));
            return this;
        };

        $('#package-date, #expiry-date').datepicker({
            dateFormat : 'dd-mm-yy'
        });

        $('input').attr('autocomplete', 'off');

        $('#box-type').on('change', function(){

            $('#case-no').val($(this).find(':selected').data('count'));
            $('#box-weight').val($(this).find(':selected').data('weight'));
            $('input#packing_box').val($(this).find(':selected').data('box'));

        });

        $(document).on('input','.number' ,function() {
            $(this).val($(this).val().replace(/[^0-9]/gi, ''));
        });

        $(document).on('input','.decimal' ,function() {
            $(this).val($(this).val().replace(/[^0-9.]/gi, ''));
        });

        var socket = io('http://127.0.0.1:3000');

        socket.on('laravel_database_weightlog-channel:weightLogEvent', function(data){
            $('#g_wt').val(data);
        });

        $('#expiry-date').on('keyup',function() {
          $(this).val("");
        });

        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        // $('#create-package').on('click', function(){

        //     // var validation = false;
        //     // $('.required').each(function(){
        //     //     if($(this).val() == ''){
        //     //         validation = true;
        //     //         $(this).parent().find('.text-danger').remove();
        //     //         $(this).parent().append('<span class = "text-danger"><strong>Field Required</strong></span>');
        //     //     }
        //     // })

        //     var boxType = $('#box-type').val();
        //     var caseNo = $('#case-no').val();
        //     var info = $('#info').val();
        //     var packageType = $('#package-type').val();
        //     var packageDate = $('#package-date').val();
        //     var bobbinCount = $('#bobbin-count').val();
        //     var moistureRegain = $('#moisture-regain').val();
        //     var packingBox = $('#box-type').find('option:selected').data('box');
        //     var boxWeight = $('#box-weight').val();
        //     var weightLog = '';
        //     var dispatchDate = $('#dispatch-date').val();
        //     var expiryDate = $('#expiry-date').val();

        //     $('.wl-entry').each(function(){
        //         weightLog += $(this).data('id') + '|';
        //     })

        //     var insetWeight = $('#inset-weight').val();

        //     var dataString = 'material_type=normal&box_type='+boxType+'&case_no='+caseNo+'&info='+info+'&package_type='+packageType+'&package_date='+packageDate+'&bobbin_count='+bobbinCount+'&weight_log='+weightLog+'&moisture_regain='+moistureRegain+'&packing_box='+packingBox+'&box_weight='+boxWeight+'&dispatch_date='+dispatchDate+'&expiry_date='+expiryDate+'&inset_weight='+insetWeight;


        //     if(validation){
        //         alert('Please fill all the required Fields');
        //     }else{
        //         console.log(dataString);
        //         $.ajax({
        //             type:'POST',
        //             url:'/add-package-details',
        //             data: dataString,
        //             // cache:false,
        //             // processData:false,
        //             success:function(data, status, xhr){
        //                 console.log(data);
        //                 data = JSON.parse(data.data)

        //                 if (data.status) {
        //                     $('#response').text(data.response);
        //                     $('#responsePopup').modal('show');
        //                 }else{
        //                     $('#response').text(data.response);
        //                     $('#responsePopup').modal('show');
        //                 }
        //             },
        //             error:function(xhr){
        //                 // if (xhr.status == 422) {
        //                 //     // errorHandler(xhr.responseJSON.errors)
        //                 //     console.log(xhr.responseJSON.errors);
        //                 // }
        //             }
        //         })

        //     }

        // });


        var lastEntry = {!! json_encode($lastEntry) !!}
        var manufacturedDate = {!! json_encode($manufacturedDate) !!}

        var packedDate = new Date(lastEntry.packed_date);

        if(lastEntry.expiry_date != null){
            var expiryDate = new Date(lastEntry.expiry_date);
            expiryDate = ('0'+expiryDate.getDate()).slice(-2)+'-'+('0'+(expiryDate.getMonth()+1)).slice(-2)+'-'+expiryDate.getFullYear();
        }else{
            var expiryDate = lastEntry.expiry_date;

        }





        packedDate = ('0'+packedDate.getDate()).slice(-2)+'-'+('0'+(packedDate.getMonth()+1)).slice(-2)+'-'+packedDate.getFullYear();

        if(lastEntry != null){
            console.log(lastEntry);
            var availableBoxes = $('#box-type').find('option[data-box="'+lastEntry.packing_box+'"]');

            availableBoxes.each(function(){
                if($(this).attr('data-pack') == lastEntry.box_type){
                    $('#box-type option').eq($(this).index()).prop('selected',true);
                }
            })
            $('#moisture-regain').on('keydown', function(e){e.preventDefault()});

            if(lastEntry.moisture_weight == 0){
                $('#moisture-regain').val(0);

            }else{

            }

            $('#case-no').val($('#box-type').find('option:selected').data('count'));
            
            $('input#packing_box').val($('#box-type').find(':selected').data('box'));

            $('#package-type').val(lastEntry.package_type).change();
            $('#package-date').val(packedDate);
            $('#packing-box').val(lastEntry.packing_box).change();
            $('#moisture-regain').val(lastEntry.moisture_weight);
            $('#dispatch-date').val(lastEntry.dispatch_date).change();
            $('#dispatch-date').val(lastEntry.dispatch_date);
            $('#expiry-date').val(expiryDate);
            $('#inset-weight').val(lastEntry.inset_weight).change();
            $('#info').val(lastEntry.reason);
            $('#box-weight').val(lastEntry.box_weight);

            // $('#package-date').data('dom-data', las)

            // $('#bobbin-count').val(lastEntry.bobbin_count);

        }


        $('.required').on('keydown', function(){
           $(this).parent().find('.text-danger').remove();
        });



        $('#expiry-time').on('change', function(){

            var packedDate = $('#dom').text().split('-');
            packedDate = new Date(packedDate[2], packedDate[1] -1, packedDate[0])

            packedDate = packedDate.addMonths((parseInt($(this).val())) - 1).month();

            var expiryDate = packedDate.getDate()+'-'+('0'+(packedDate.getMonth()+1)).slice(-2)+'-'+packedDate.getFullYear()

            $('#expiry-date').val(expiryDate);


        });


        $('.change-moisture').on('click', function(){
            $('#moisturePopup').modal('show');
        });


        $('#set-moisture-form').on('submit', function(e){
           e.preventDefault();

            $.ajax({
                type:'POST',
                url:'/check-password',
                data: 'password='+$(this).find('#password').val(),
                // cache:false,
                // processData:false,
                success:function(data, status, xhr){
                    if(data.message == true){
                        // console.log($('#moisture').val());
                        $('#moisture-regain').val($('#moisture').val());
                        $('#moisturePopup').modal('hide');
                        $('#set-moisture-form').trigger('reset');
                    }else{
                        $('#moisture-response').text('Wrong Supervisor Password. Please try with correct Password.').show();
                        // $('#moisturePopup').modal('hide');
                        // $('#responsePopup').modal('show');
                    }
                },
                error:function(xhr){
                    // if (xhr.status == 422) {
                    //     // errorHandler(xhr.responseJSON.errors)
                    //     console.log(xhr.responseJSON.errors);
                    // }
                }
            })
        });


        $('#password').on('keydown', function(){
            $('#moisture-response').hide();
        })

        $('.add-doff').on('click',function(){
            $('.doff-unit').append('<div class="col-md-3">'+
                    '<input type="hidden" name="new_material[]" id="material-'+doff_count+'">'+
                        '<div class="form-group">'+
                            '<label for="doff_no">Doff No</label>'+
                            '<select name="doff_no[]" class="doff_no" data-val="'+doff_count+'" required>'+

                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="material_id">Material</label>'+
                            '<select name="material_id[]" id="material_id-'+doff_count+'" class="material_id" data-val="'+doff_count+'" required>'+
                                '<option value="">Material</option>'+

                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="filament">Filament Type:</label>'+
                            '<select name="filament[]" id="filament-'+doff_count+'" required>'+
                                '<option value="">Filament</option>'+
                            '</select>'+
                        '</div>'+
                    '</div>'+
                    '<div class="col-md-3">'+
                        '<div class="form-group">'+
                            '<label for="b_count">Bobbin Count <span id="count-'+doff_count+'"></span></label>'+
                            '<input type="text" class="text-input number required" class="b_count" name="b_count[]" required>'+
                        '</div>'+
                    '</div>');
            doff_count++;
            $('.doff_no').empty();
            $('.doff_no').append('<option value="" selected disabled>Select Doff</option>');
            $.each(doffs,function(doff,mat){
                    $('.doff_no').append('<option value="'+doff+'">'+doff+'</option>');
            });
        });

        $(document).on('change','.doff_no',function(){
            var doff_no = $(this).val();
            var id_count = $(this).data('val');
            var material_op = $('#material_id-'+id_count);
            var filament_op = $('#filament-'+id_count);
            material_op.empty();
            filament_op.empty(); 
            material_op.append('<option value="" disabled selected>Material</option>');           
            filament_op.append('<option value="" disabled selected>Filament</option>');           
            $.each(doffs,function(key,val){
               if(doff_no == key) {
                $.each(val.materials,function(key2,val2){
                    material_op.append('<option value="'+val2.material_id+'" data-count="'+val2.count+'">'+val2.material+'</option>');
                });
                $.each(val.filaments,function(key3,val3){
                    filament_op.append('<option value="'+val3+'">'+val3+'</option>');
                });
                $('#dom').text(val.dom);
               }
            });
        });

        $(document).on('change','.material_id',function(){
            var val = $(this).data('val');
            var span_id = $('#count-'+val);
            var material = $('#material-'+val);
            material.val($(this).find('option:selected').text())
            span_id.text(" - "+$(this).find('option:selected').data('count'));
        });
    });

</script>

</body>
</html>
