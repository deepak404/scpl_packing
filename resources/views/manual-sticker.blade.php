<html>
<head>
    <title>Inspection</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>
        #main{
            margin-top: 100px;
        }

        .wl-card{
            width: 100%;
            height: 400px;
            /*box-shadow: 0px 0px 5px rgba(0,0,0, 0.2);*/
            /*margin-top: 10%;*/
        }


        .text-danger{
            color: #ff1744;
            margin-top: 10px;
            display: inline-block;
        }

        #ui-datepicker-div{
            width: 250px;
        }

        th{
            text-align: center;
        }

        tr>td:nth-child(3){
            width: 4%;
        }

        tr>td:nth-child(2){
            width: 10%;
        }

        tr>td:nth-child(4){
            width: 8%;
        }

        tr>td:nth-child(1){
            width: 10%;
        }

        tr>td{
            text-align: center;
        }

        .wl-card>h3{
            margin-top: 20px;
            margin-bottom: 20px;

        }

        select:disabled{
            background-color: gainsboro !important;
        }

        .form-group{
            margin-bottom: 20px;
        }

    </style>

    <div id="loader" class="loader"></div>
    <section id="header">
            <header>
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar-collapse" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                            </div>
        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a class="" href="/home">Packing</a></li>
                                    <li><a href="/ncr-packing">NCR Packing</a></li>
                                    <li><a href="/leader-packing">Leader Packing</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle active-menu" data-toggle="dropdown" role="button"
                                        data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Braiding <i class="material-icons">
                                            arrow_drop_down
                                        </i></a>
                                        <ul class="dropdown-menu">
                                            <li><a class="active-menu" href="/braiding">Regular</a></li>
                                            <li><a class="" href="#">NCR</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown active">
                                            <a href="#" class="dropdown-toggle active-menu" data-toggle="dropdown" role="button"
                                            data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Others <i class="material-icons">
                                                arrow_drop_down
                                            </i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="/get-packed-cases">Packed Cases</a></li>
                                                <li class="active"><a class="active-menu" href="/print-sticker-range">Manual Sticker</a></li>
                                                <li><a href="/re-packing">Re-Packing</a></li>
        
                                            </ul>
                                        </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>

    </section>
</head>
<body>

<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12" style="width: 500px;">
                <?php //dd(json_decode($count)->bCount); ?>

                    <h3 style="margin-left: 15px;">Generate Packing Stickers</h3>
                    <form action="/generate-manual-stickers" id="generate-sticker" name="generate-sticker" method="POST" target="_blank">
                        {{csrf_field()}}
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="from-case-no">From Case No</label>
                                <input type="text" id="from-case-no" name="from-case-no" class="text-input" required>
                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=to-case-no">To Case No</label>
                                <input type="text" id="to-case-no" name="to-case-no" class="text-input" required>

                            </div>
                        </div>

                        <input type="submit" class="btn btn-primary" style="margin-left: 15px;">
                    </form>

            </div>
        </div>
    </div>


</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>



<script>


    $(document).ready(function(){


    });

</script>

</body>
</html>
