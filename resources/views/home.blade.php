<html>
<head>
    <title>Packaging</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>
        #main{
            margin-top: 100px;
        }

        .wl-card{
            width: 100%;
            height: 400px;
            /*box-shadow: 0px 0px 5px rgba(0,0,0, 0.2);*/
            /*margin-top: 10%;*/
        }


        .text-danger{
            color: #ff1744;
            margin-top: 10px;
            display: inline-block;
        }

        #ui-datepicker-div{
            width: 250px;
        }

        th{
            text-align: center;
        }

        tr>td:nth-child(3){
            width: 4%;
        }

        tr>td:nth-child(2){
            width: 10%;
        }

        tr>td:nth-child(4){
            width: 8%;
        }

        tr>td:nth-child(1){
            width: 10%;
        }

        tr>td{
            text-align: center;
        }

        .wl-card>h3{
            margin-top: 20px;
            margin-bottom: 20px;

        }

        select:disabled{
            background-color: gainsboro !important;
        }

        .form-group{
            margin-bottom: 20px;
        }

    </style>

    <div id="loader" class="loader"></div>
    <section id="header">
            <header>
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar-collapse" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                            </div>
        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a class="active-menu" href="/home">Packing</a></li>
                                    <li><a href="/ncr-packing">NCR Packing</a></li>
                                    <li><a href="/leader-packing">Leader Packing</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Braiding <i class="material-icons">
                                            arrow_drop_down
                                        </i></a>
                                        <ul class="dropdown-menu">
                                            <li><a class="active-menu" href="/braiding">Regular</a></li>
                                            <li><a class="" href="#">NCR</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                            data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Others <i class="material-icons">
                                                arrow_drop_down
                                            </i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="/get-packed-cases">Packed Cases</a></li>
                                                <li><a href="/print-sticker-range">Manual Sticker</a></li>
                                                <li><a href="/re-packing">Re-Packing</a></li>
        
                                            </ul>
                                        </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>

    </section>
</head>
<body>

<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <?php //dd(json_decode($count)->bCount); ?>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="box-type">Box and Packing Type</label>
                        <select name="box-type" id="box-type" required>

                            <option value="C" data-pack="C" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">C 21"X21"X12.5"</option>
                            <option value="CS" data-pack="CS" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">CS 21"X21"X12.5"</option>
                            <option value="CIS" data-pack="CIS" data-box="CORRUGATED BOX 2" data-weight="1.6" data-count="{{$count->cCount}}">CIS 22"X21"X13.5"</option>
                            <option value="CKS" data-pack="CKS" data-box="CORRUGATED BOX 7" data-weight="1.6" data-count="{{$count->cCount}}">CKS 21"X21"X12.5"</option>

                            <option value="B" data-pack="B" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">B 24"X12"X13"</option>
                            <option value="B" data-pack="B" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">B 24"X12"X14"</option>
                            <option value="B" data-pack="B" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">B 25"X12.5"X15"</option>

                            <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">BS 24"X12"X13"</option>
                            <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">BS 24"X12"X14"</option>
                            <option value="BS" data-pack="BS" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">BS 25"X12.5"X15"</option>

                            <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 5" data-weight="1.0" data-count="{{$count->bCount}}">BIS 24"X12"X13"</option>
                            <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 6" data-weight="1.1" data-count="{{$count->bCount}}">BIS 24"X12"X14"</option>
                            <option value="BIS" data-pack="BIS" data-box="CORRUGATED BOX 8" data-weight="1.2" data-count="{{$count->bCount}}">BIS 25"X12.5"X15"</option>

                            <option value="BYIS" data-pack="BYIS" data-box="CORRUGATED BOX 2" data-weight="1.6" data-count="{{$count->byCount}}">BYIS 22"X21"X13.5"</option>
                            <option value="BYIS" data-pack="BYIS" data-box="CORRUGATED BOX 3" data-weight="1.4" data-count="{{$count->byCount}}">BYIS 17.5"X15.5"X18.5"</option>


                            <option value="BYS" data-pack="BYS" data-box="CORRUGATED BOX 4" data-weight="1.6" data-count="{{$count->byCount}}">BYS 20.5"X20.5"X13.5"</option>


                            {{--<option value="B" data-count="{{$count->bCount}}">B</option>--}}
                            {{--<option value="BI" data-count="{{$count->bCount}}">BI</option>--}}
                            {{--<option value="BIS" data-count="{{$count->bCount}}">BIS</option>--}}
                            {{--<option value="BS" data-count="{{$count->bCount}}">BS</option>--}}

                            {{--<option value="C" data-count="{{$count->cCount}}">C</option>--}}
                            {{--<option value="CI" data-count="{{$count->cCount}}">CI</option>--}}
                            {{--<option value="CIS" data-count="{{$count->cCount}}">CIS</option>--}}
                            {{--<option value="CS" data-count="{{$count->cCount}}">CS</option>--}}
                            {{--<option value="CK" data-count="{{$count->cCount}}">CK</option>--}}
                            {{--<option value="CKS" data-count="{{$count->cCount}}">CKS</option>--}}

                            {{--<option value="BYS" data-weight = "1.6" data-count="{{$count->byCount}}">BYS</option>--}}
                            {{--<option value="BYIS" data-weight = data-count="{{$count->byCount}}">BYIS</option>--}}

                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="case-no">Case No</label>
                        <input type="text" id="case-no" name="case-no" class="text-input required">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="info">Remarks</label>
                        <input type="text" id="info" name="info" class="text-input">
                    </div>
                </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="moisture-regain">MR <a class="change-moisture" style="font-size: 14px; float: right; color: #003EBB;" >Change</a></label>
                            <input type="text" id="moisture-regain" name="moisture-regain" class="text-input required" required>
                        </div>
                    </div>
                {{--<div class="col-md-3">--}}
                    {{--<div class="form-group">--}}
                        {{--<label for="packing-box">Packing Box</label>--}}
                        {{--<select id="packing-box" name="packing-box" class="text-input">--}}
                            {{--@foreach($packingResult as $packing)--}}

                                {{--<option value="{{$packing['name']}}" data-weight="{{$packing['value']}}">{{$packing['name']." - ".$packing['dimension']}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="col-md-12 col-sm-12 col-lg-12" style="padding: 0px;">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="box-weight">Box Weight</label>
                            <input type="text" id="box-weight" name="box-weight" class="text-input required" value="1.6">
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="package-type">Package Type</label>
                            <select id="package-type" name="package-type" class="text-input" required>
                                <option value="BOBBIN">BOBBIN</option>
                                <option value="CHEESE">CHEESE</option>
                                <option value="BRAIDINGYARN">BRAIDING YARN</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="package-date">Package Date | DOM : <span id="dom"></span></label>
                            <input type="text" id="package-date" name="package-date" class="text-input required" required>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="bobbin-count">Bobbin Count</label>
                            <input type="text" id="bobbin-count" name="bobbin-count" class="text-input required" required value="0">
                        </div>
                    </div>
                </div>




                <div class="col-md-3">
                    <div class="form-group">
                        <label for="dispatch-date">Dispatch Date</label>
                        <select id="dispatch-date" name="dispatch-date" class="text-input required">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="expiry-time">Expiry Time</label>
                        <select type="text" id="expiry-time" name="expiry-time" class="text-input">
                            <option value="" disabled selected>--</option>
                            <option value="3">3 Months</option>
                            <option value="6">6 Months</option>
                            <option value="9">9 Months</option>
                            <option value="12">1 Year</option>
                        </select>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        <label for="expiry-date">Expiry Date</label>
                        <input type="text" id="expiry-date" name="expiry-date" class="text-input">
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="form-group">
                        <label for="inset-weight">Inset Weight</label>
                        {{--<input type="text" id="inset-weight" name="inset-weight" class="text-input required">--}}
                        <select name="inset-weight" id="inset-weight" class="text-input">
                            <option value="0">0</option>
                            <option value="0.1">0.10</option>
                            <option value="0.2">0.20</option>
                            <option value="0.3">0.30</option>
                            <option value="0.4">0.40</option>
                            <option value="0.5">0.50</option>
                            <option value="0.6">0.60</option>
                            <option value="0.7">0.70</option>
                            <option value="0.8">0.80</option>
                            <option value="0.9">0.90</option>
                            <option value="1">1</option>

                        </select>
                    </div>
                </div>

                <div class="wl-card col-md-12">
                    <h3>Packing</h3>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Material</th>
                            <th>Filament</th>
                            <th>DOFF No</th>
                            <th>Spindle</th>
                            <th>Total Weight (kg)</th>
                            <th>Tare Weight(kg)</th>
                            <th>Material Weight(kg)</th>
                            <th>Machine</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    <input type="button" class="btn btn-primary center-block" id="create-package" style="width: 10%;" value="Create Package">

                </div>
            </div>
        </div>
    </div>


    <div id="responsePopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Inspection Update Status</h4>
                </div>
                <div class="modal-body">
                    <h4 id="response"  style="margin-bottom: 20px;"></h4>
                    <input type="button" class="btn btn-primary center-block" style="width: 100px" onClick="window.location.reload()" value="Done">
                </div>
            </div>

        </div>
    </div>


    <div id="moisturePopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Supervisor MR Change.</h4>
                </div>
                <div class="modal-body">
                    <form action="#" name="set-moisture-form" id="set-moisture-form">
                        <div class="form-group">
                            <label for="moisture">M Value</label>
                            <input type="number" name="moisture" class="text-input" id="moisture" step="0.1">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" name="password" class="text-input" id="password">
                        </div>

                        <div class="form-group">
                            <input type="submit" name="password" class="btn btn-primary" value="Change Moisture">
                        </div>
                        <p id="moisture-response" class="text-danger" style="padding: 5px; display: none;"></p>
                    </form>
                </div>
            </div>

        </div>
    </div>

</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>


<script>


    $(document).ready(function(){


        Date.isLeapYear = function (year) {
            return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
        };

        Date.getDaysInMonth = function (year, month) {
            return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
        };

        Date.prototype.isLeapYear = function () {
            return Date.isLeapYear(this.getFullYear());
        };

        Date.prototype.getDaysInMonth = function () {
            return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
        };

        Date.prototype.addMonths = function (value) {
            var n = this.getDate();
            this.setDate(1);
            this.setMonth(this.getMonth() + value);
            this.setDate(Math.min(n, this.getDaysInMonth()));
            return this;
        };

        $('#package-date, #expiry-date').datepicker({
            dateFormat : 'dd-mm-yy'
        });

        $('#box-type').on('change', function(){

            $('#case-no').val($(this).find(':selected').data('count'));
            $('#box-weight').val($(this).find(':selected').data('weight'));

        });


        // $('#packing-box').on('change', function(){
        //
        //     $('#box-weight').val($(this).find(':selected').data('weight'));
        // });

        $('#expiry-date').on('keyup',function() {
          $(this).val("");
        });

        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        $('#create-package').on('click', function(){

            var validation = false;
            $('.required').each(function(){
                if($(this).val() == ''){
                    validation = true;
                    $(this).parent().find('.text-danger').remove();
                    $(this).parent().append('<span class = "text-danger"><strong>Field Required</strong></span>');
                }
            })

            var boxType = $('#box-type').val();
            var caseNo = $('#case-no').val();
            var info = $('#info').val();
            var packageType = $('#package-type').val();
            var packageDate = $('#package-date').val();
            var bobbinCount = $('#bobbin-count').val();
            var moistureRegain = $('#moisture-regain').val();
            var packingBox = $('#box-type').find('option:selected').data('box');
            var boxWeight = $('#box-weight').val();
            var weightLog = '';
            var dispatchDate = $('#dispatch-date').val();
            var expiryDate = $('#expiry-date').val();

            $('.wl-entry').each(function(){
                weightLog += $(this).data('id') + '|';
            })

            var insetWeight = $('#inset-weight').val();

            var dataString = 'material_type=normal&box_type='+boxType+'&case_no='+caseNo+'&info='+info+'&package_type='+packageType+'&package_date='+packageDate+'&bobbin_count='+bobbinCount+'&weight_log='+weightLog+'&moisture_regain='+moistureRegain+'&packing_box='+packingBox+'&box_weight='+boxWeight+'&dispatch_date='+dispatchDate+'&expiry_date='+expiryDate+'&inset_weight='+insetWeight;


            if(validation){
                // alert('Please fill all the required Fields');
            }else{
                console.log(dataString);
                $.ajax({
                    type:'POST',
                    url:'/add-package-details',
                    data: dataString,
                    // cache:false,
                    // processData:false,
                    success:function(data, status, xhr){
                        console.log(data);
                        data = JSON.parse(data.data)

                        if (data.status) {
                            $('#response').text(data.response);
                            $('#responsePopup').modal('show');
                        }else{
                            $('#response').text(data.response);
                            $('#responsePopup').modal('show');
                        }
                    },
                    error:function(xhr){
                        // if (xhr.status == 422) {
                        //     // errorHandler(xhr.responseJSON.errors)
                        //     console.log(xhr.responseJSON.errors);
                        // }
                    }
                })

            }

        });

        function  showBarcodeData(data){

            data = JSON.parse(data);
            wl = data.data;
            console.log(data.status, data.data)
            if(data.status){
                // $('tbody').empty();
                //  console.log(jQuery.type(data.data));
                // console.log(select.outerHTML)
                $.each(data.data, function(key, wl){
                    console.log(key, wl);
                    if(wl.inspection_status == 1 && wl.packed_status != 1 && wl.active_status == 1 && wl.ncr_status == 0){
                        $('tbody').append(
                            '<tr data-id = '+wl.id+' class="wl-entry">'+
                            '<td>'+wl.material+'</td>'+
                            '<td>'+wl.filament+'</td>'+
                            '<td>'+wl.doff_no+'</td>'+
                            '<td>'+wl.spindle+'</td>'+
                            '<td>'+wl.total_weight+'</td>'+
                            '<td>'+wl.tare_weight+'</td>'+
                            '<td>'+wl.material_weight+'</td>'+
                            '<td>'+wl.machine+'</td>'+
                            '<td><a class="remove-wl"><i class="material-icons">delete</i> </a></td>'+
                            '</tr>'
                        );

                        $('#bobbin-count').val(parseInt($('#bobbin-count').val()) + 1);

                        var dom = new Date(wl.doff_date);
                        dom = ('0'+dom.getDate()).slice(-2)+'-'+('0'+(dom.getMonth()+1)).slice(-2)+'-'+dom.getFullYear();

                        $('#dom').text(dom);
                    }else if(wl.packed_status == 1){
                        alert('The Spindle is already packed');
                    }else if(wl.ncr_status == 1){

                        alert('This Spindle is NCR');
                    }else if(wl.inspection_status == 0){
                        alert('The Spindle is not Inspected yet.');
                    }

                });
            }else{
                alert('No data found for this QR Code');

            }
        }


        $(document).on('click','.remove-wl', function(){
            $(this).parent().parent().remove();

            if(parseInt($('#bobbin-count').val()) != 0){
                $('#bobbin-count').val(parseInt($('#bobbin-count').val())- 1);
            }
        });

        var lastEntry = {!! json_encode($lastEntry) !!}
        var manufacturedDate = {!! json_encode($manufacturedDate) !!}

        var packedDate = new Date(lastEntry.packed_date);

        if(lastEntry.expiry_date != null){
            var expiryDate = new Date(lastEntry.expiry_date);
            expiryDate = ('0'+expiryDate.getDate()).slice(-2)+'-'+('0'+(expiryDate.getMonth()+1)).slice(-2)+'-'+expiryDate.getFullYear();
        }else{
            var expiryDate = lastEntry.expiry_date;

        }





        packedDate = ('0'+packedDate.getDate()).slice(-2)+'-'+('0'+(packedDate.getMonth()+1)).slice(-2)+'-'+packedDate.getFullYear();

        if(lastEntry != null){
            console.log(lastEntry);
            var availableBoxes = $('#box-type').find('option[data-box="'+lastEntry.packing_box+'"]');

            availableBoxes.each(function(){
                if($(this).attr('data-pack') == lastEntry.box_type){
                    $('#box-type option').eq($(this).index()).prop('selected',true);
                }
            })
            $('#moisture-regain').on('keydown', function(e){e.preventDefault()});

            if(lastEntry.moisture_weight == 0){
                $('#moisture-regain').val(0);

            }else{

            }

            $('#case-no').val($('#box-type').find('option:selected').data('count'));

            $('#package-type').val(lastEntry.package_type).change();
            $('#package-date').val(packedDate);
            $('#packing-box').val(lastEntry.packing_box).change();
            $('#moisture-regain').val(lastEntry.moisture_weight);
            $('#dispatch-date').val(lastEntry.dispatch_date).change();
            $('#dispatch-date').val(lastEntry.dispatch_date);
            $('#expiry-date').val(expiryDate);
            $('#inset-weight').val(lastEntry.inset_weight).change();
            $('#info').val(lastEntry.reason);
            $('#box-weight').val(lastEntry.box_weight);
            // $('#package-date').data('dom-data', las)

            // $('#bobbin-count').val(lastEntry.bobbin_count);

        }


        $('.required').on('keydown', function(){
           $(this).parent().find('.text-danger').remove();
        });
        // console.log(lastEntry);

        var socket = io('http://127.0.0.1:3000');

        socket.on('barcode-channel:barcodeEvent', function(data){

            console.log(data);
            showBarcodeData(data)
        });


        $('#expiry-time').on('change', function(){

            var packedDate = $('#dom').text().split('-');
            packedDate = new Date(packedDate[2], packedDate[1] -1, packedDate[0])

            packedDate = packedDate.addMonths((parseInt($(this).val())) - 1).month();

            var expiryDate = packedDate.getDate()+'-'+('0'+(packedDate.getMonth()+1)).slice(-2)+'-'+packedDate.getFullYear()

            $('#expiry-date').val(expiryDate);


        });


        $('.change-moisture').on('click', function(){
            $('#moisturePopup').modal('show');
        });


        $('#set-moisture-form').on('submit', function(e){
           e.preventDefault();

            $.ajax({
                type:'POST',
                url:'/check-password',
                data: 'password='+$(this).find('#password').val(),
                // cache:false,
                // processData:false,
                success:function(data, status, xhr){
                    if(data.message == true){
                        // console.log($('#moisture').val());
                        $('#moisture-regain').val($('#moisture').val());
                        $('#moisturePopup').modal('hide');
                        $('#set-moisture-form').trigger('reset');
                    }else{
                        $('#moisture-response').text('Wrong Supervisor Password. Please try with correct Password.').show();
                        // $('#moisturePopup').modal('hide');
                        // $('#responsePopup').modal('show');
                    }
                },
                error:function(xhr){
                    // if (xhr.status == 422) {
                    //     // errorHandler(xhr.responseJSON.errors)
                    //     console.log(xhr.responseJSON.errors);
                    // }
                }
            })
        });


        $('#password').on('keydown', function(){
            $('#moisture-response').hide();
        })

    });

</script>

</body>
</html>
