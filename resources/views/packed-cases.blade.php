<html>
<head>
    <title>Packaging</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link rel="stylesheet" href="{{url('css/generate-indent.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.20/jquery.datetimepicker.css" />

    <style>
        #main{
            margin-top: 100px;
        }


        form{
            margin-top: 0px;
            margin-bottom: 0px;
        }


        #reason{
            width: 50%;
        }

        input[type = "submit"]{
            width: 100px !important;
        }

        .wl-card{
            width: 100%;
            height: 400px;
            /*box-shadow: 0px 0px 5px rgba(0,0,0, 0.2);*/
            /*margin-top: 10%;*/
        }


        .text-danger{
            color: #ff1744;
            margin-top: 10px;
            display: inline-block;
        }

        #ui-datepicker-div{
            width: 250px;
        }

        th{
            text-align: center;
        }

        tr>td:nth-child(3){
            width: 4%;
        }

        tr>td:nth-child(2){
            width: 10%;
        }

        tr>td:nth-child(4){
            width: 8%;
        }

        tr>td:nth-child(1){
            width: 10%;
        }

        tr>td{
            text-align: center;
        }

        .wl-card>h3{
            margin-top: 20px;
            margin-bottom: 20px;

        }

        select:disabled{
            background-color: gainsboro !important;
        }

        .form-group{
            margin-bottom: 20px;
        }


        .panel-default{
            padding-left: 0px;
            padding-right: 0px;
            margin-bottom: 20px !important;
        }

        .panel-heading{
            padding-bottom: 10px;
            margin-bottom: 10px;
            padding-top: 10px;
            height: 60px;
        }


        .pack-heading span, .pack-detail span{
            width: 14%;
            display: inline-block;
        }

        .pack-heading span{
            font-weight: 600;
        }

        .pack-heading span:last-child, .pack-detail span:last-child{
            width: 9%;
        }

        .panel-default>.panel-heading{
            height: 140px;
        }

        .pagination>.active>span{
            padding: 9.5px 10px;
            background-color: #003EBB;
        }

        .pagination>.disabled>span{
            padding: 9.5px 10px;

        }

        .pagination>li>a, .pagination>li>span{
            color: #003EBB;
        }

        .edit-package .material-icons{
            font-size: 18px !important;
            vertical-align: middle;
        }

    </style>

    <div id="loader" class="loader"></div>
    <section id="header">
            <header>
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar-collapse" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                            </div>
        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="/home">Packing</a></li>
                                    <li><a href="/ncr-packing">NCR Packing</a></li>
                                    <li><a href="/leader-packing">Leader Packing</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Braiding <i class="material-icons">
                                            arrow_drop_down
                                        </i></a>
                                        <ul class="dropdown-menu">
                                            <li><a class="active-menu" href="/braiding">Regular</a></li>
                                            <li><a class="" href="#">NCR</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown active">
                                            <a href="#" class="dropdown-toggle  active-menu" data-toggle="dropdown" role="button"
                                            data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Others <i class="material-icons">
                                                arrow_drop_down
                                            </i></a>
                                            <ul class="dropdown-menu">
                                                <li class="active"><a class="active-menu" href="/get-packed-cases">Packed Cases</a></li>
                                                <li><a href="/print-sticker-range">Manual Sticker</a></li>
                                                <li><a href="/re-packing">Re-Packing</a></li>
        
                                            </ul>
                                        </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>

    </section>
</head>
<body>

<section id="main">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="panel-group col-md-12 col-lg-12 col-sm-12" id="accordion" role="tablist" aria-multiselectable="true">

                    @foreach($packages as $package)
                        <div class="panel panel-default col-md-12 col-lg-12 col-sm-12">
                            <div class="panel-heading col-md-12 col-lg-12 col-sm-12" role="tab" id="headingOne">


                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Case No</th>
                                            <th>Packed Date</th>
                                            <th style="width: 20%">Package Type</th>
                                            <th>Box Type</th>
                                            <th>Bobbin Count</th>
                                            <th>Material Type</th>
                                            <th>Expiry date</th>
                                            <th>Reason</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$package->case_no}}</td>
                                            <td>{{date('d-m-Y', strtotime($package->packed_date))}}</td>
                                            <td>{{$package->package_type}}</td>
                                            <td>{{$package->packing_box}}</td>
                                            <td>{{$package->bobbin_count}}</td>
                                            
                                            <td>{{$package->material_type}}</td>
                                            @if($package->expiry_date != null)
                                            <td>{{date('d-m-Y', strtotime($package->expiry_date))}}</td>
                                            @else
                                                <td>--</td>
                                            @endif
                                            @if($package->reason != null)
                                                <td><strong>{{$package->reason}}</strong> <span><a data-id="{{$package->id}}" href="#" class="edit-package"><i class="material-icons">edit</i></a></span></td>
                                            @else
                                                <td><span><a href="#" class="edit-package" data-id="{{$package->id}}"><i class="material-icons">edit</i></a></span></td>
                                            @endif
                                            <td>
                                                <a class="print-qr" data-id="{{$package->id}}"><i class="material-icons">print</i></a>
                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">{{--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.--}}
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Material</th>
                                                <th>DOFF No</th>
                                                <th style="width: 20%;">Spindle</th>
                                                <th style="width: 20%;">Material Weight</th>
                                                <th>Total Weight</th>
                                                <th>Tare Weight</th>
                                            </tr>
                                        </thead>
                                        <tbody>


                                            @foreach($package->weight_logs as $wl)
                                                <tr>
                                                    <td>{{$wl->material}}</td>
                                                    <td>{{$wl->doff_no}}</td>
                                                    <td>{{$wl->spindle}}</td>
                                                    <td>{{$wl->material_weight}}</td>
                                                    <td>{{$wl->total_weight}}</td>
                                                    <td>{{$wl->tare_weight}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    @endforeach
                       <div class="text-center col-md-12 col-lg-12 col-sm-12">
                           {{-- {!! $packages->render() !!} --}}
                            <ul class="pagination">
                                @if ($currentPage > 4)
                                    <li><a href="/get-packed-cases/1">1</a></li>                                        
                                    <li class="disabled"><a href="#">..</a></li>
                                @endif
                                @foreach ($pageArray as $item)
                                    @if ($item == $currentPage)
                                        <li class="active"><a href="/get-packed-cases/{{$item}}">{{$item}}</a></li>
                                    @else
                                        <li><a href="/get-packed-cases/{{$item}}">{{$item}}</a></li>                                        
                                    @endif
                                @endforeach
                                @if ($item < $lastPage)
                                    <li class="disabled"><a href="#">..</a></li>
                                    <li><a href="/get-packed-cases/{{$lastPage}}">{{$lastPage}}</a></li>                                        
                                @endif
                            </ul>
                       </div>

                </div>
            </div>
        </div>
    </div>

    <div id="editPopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Reason</h4>
                </div>
                <div class="modal-body">
                    <form action="/edit-package" method="POST" id="edit-package-form">

                        <input type="hidden" name="package-id" id="hidden-p-id">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="reason">Reason</label>
                            <input type="text" id="reason" name="reason" class="text-input" required>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" value="submit">
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>


    <div id="responsePopup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Package Update Status</h4>
                </div>
                <div class="modal-body">
                    <h4 id="response"  style="margin-bottom: 20px;"></h4>
                    <input type="button" class="btn btn-primary center-block" style="width: 100px" onClick="window.location.reload()" value="Done">
                </div>
            </div>

        </div>
    </div>

</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>


<script>


    $(document).ready(function(){


        $('.edit-package').on('click', function(e){
            e.preventDefault();
            $('#hidden-p-id').val($(this).data('id'));
            $('#editPopup').modal('show');
            console.log()
        })



        $('#edit-package-form').on('submit', function(e){
            e.preventDefault()

            $.ajax({
                type:'POST',
                url:'/edit-package',
                data: $(this).serialize(),
                // cache:false,
                // processData:false,
                success:function(data, status, xhr){

                    $('#editPopup').modal('hide');
                    $('#response').text(data.response.response);
                    $('#responsePopup').modal('show');
                },
                error:function(xhr){

                }
            })

        })

        $('.print-qr').on('click', function(){

            $.ajax({
                type:'POST',
                url:'/print-pack-qr',
                data: 'id='+$(this).data('id'),
                // cache:false,
                // processData:false,
                success:function(data, status, xhr){

                },
                error:function(xhr){

                }
            })

        });


        $.ajaxSetup({
            headers:
                { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

    });

</script>

</body>
</html>
