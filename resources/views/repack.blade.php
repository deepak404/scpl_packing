<html>
<head>
    <title>Re-Pack</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('css/global.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <style>
        #main{
            margin-top: 100px;
        }

        .wl-card{
            width: 100%;
            height: 400px;
            /*box-shadow: 0px 0px 5px rgba(0,0,0, 0.2);*/
            /*margin-top: 10%;*/
        }


        .text-danger{
            color: #ff1744;
            margin-top: 10px;
            display: inline-block;
        }

        #ui-datepicker-div{
            width: 250px;
        }

        th{
            text-align: center;
        }

        tr>td:nth-child(3){
            width: 4%;
        }

        tr>td:nth-child(2){
            width: 10%;
        }

        tr>td:nth-child(4){
            width: 8%;
        }

        tr>td:nth-child(1){
            width: 10%;
        }

        tr>td{
            text-align: center;
        }

        .wl-card>h3{
            margin-top: 20px;
            margin-bottom: 20px;

        }

        select:disabled{
            background-color: gainsboro !important;
        }

        .form-group{
            margin-bottom: 20px;
        }

        h3{
            margin-top: 0px !important;
        }
        form{
            margin-top: 10px !important;
        }
    </style>

    <div id="loader" class="loader"></div>
    <section id="header">
            <header>
                    <nav class="navbar navbar-default navbar-fixed-top">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                        data-target="#navbar-collapse" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="/"><img src="{{url('assets/logo.svg')}}" class="nav-logo"></a>
                            </div>
        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li><a href="/home">Packing</a></li>
                                    <li><a href="/ncr-packing">NCR Packing</a></li>
                                    <li><a href="/leader-packing">Leader Packing</a></li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Braiding <i class="material-icons">
                                            arrow_drop_down
                                        </i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="/braiding">Regular</a></li>
                                            <li><a class="" href="#">NCR</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown active">
                                            <a href="#" class="dropdown-toggle active-menu" data-toggle="dropdown" role="button"
                                            data-hover="dropdown" aria-haspopup="true" aria-expanded="false">Others <i class="material-icons">
                                                arrow_drop_down
                                            </i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="/get-packed-cases">Packed Cases</a></li>
                                                <li><a href="/print-sticker-range">Manual Sticker</a></li>
                                                <li class="active"><a class="active-menu" href="/re-packing">Re-Packing</a></li>
        
                                            </ul>
                                        </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>

    </section>
</head>
<body>

    <section id="main">
        <div class="container-fluid">
            <div class="row">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <h3 style="margin-left: 15px;">Create Repacking</h3>
                    <form class="col-md-12 col-lg-12 col-sm-12" action="/create-repack" method="post">
                        @csrf
                        <input type="hidden" name="id">
                        <div class="form-group col-md-12 col-lg-12 col-sm-12">
                            <label for="doff_no">Doff No:</label>
                            <input type="text" class="form-control" name="doff_no" value="{{ old('doff_no') }}" required>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12">
                            <label for="doff_date">Doff Date:</label>
                            <input type="text" class="form-control" name="doff_date" value="{{ old('doff_date') }}" required>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12">
                            <label for="material">Material:</label>
                            <select name="material" id="material" class="form-control">
                                @foreach ($material as $item)
                                    @if (old('material') == $item->id)
                                        <option value="{{$item->id}}" selected>{{$item->material}}</option>                                    
                                    @else
                                        <option value="{{$item->id}}">{{$item->material}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12">
                            <label for="floor_code">Floor Code:</label>
                            <input type="text" class="form-control" name="floor_code" value="{{ old('floor_code') }}" required>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12">
                            <label for="spindle">Spindle:</label>
                            <input type="text" class="form-control" name="spindle" value="{{ old('spindle') }}" required>
                        </div>
                        <div class="form-group col-md-12 col-lg-12 col-sm-12">
                            <input type="submit" class="btn btn-primary center-block" value="Update">
                        </div>
                    </form>
                </div>
                <div class="col-md6 col-lg-6 col-sm-6">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Doff No</th>
                                <th>Doff Date</th>
                                <th>Spindle</th>
                                <th>Material</th>
                                <th>Floor Code</th>
                                <th>Print</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($packedEntry as $wl)
                                <tr>
                                    <td>{{$wl->doff_no}}</td>
                                    <td>{{$wl->doff_date}}</td>
                                    <td>{{$wl->spindle}}</td>
                                    <td>{{$wl->material}}</td>
                                    <td>{{$wl->floor_code}}</td>
                                    <td><i class="material-icons print" data-id="{{$wl->id}}">print</i></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script>


        $(document).ready(function(){
            $('input[name="doff_date"]').datepicker();
            $('input').attr('autocomplete','off');

            var socket = io('http://127.0.0.1:3000');

            socket.on('barcode-channel:barcodeEvent', function(data){

                showBarcodeData(data)

            });

            function  showBarcodeData(data){

                data = JSON.parse(data);
                wl = data.data;
                if(data.status){
                    $.each(data.data, function(key, wl){
                        console.log(key, wl);
                        if(wl.packed_status == 1 && wl.active_status == 1 && wl.challan_id != 0 && wl.challan_id != null){
                            $('input[name="id"]').val(wl.id);
                            $('input[name="material"]').val(wl.material_id);
                            $('input[name="doff_no"]').val(wl.doff_no);
                            $('input[name="spindle"]').val(wl.spindle);
                            $('input[name="doff_date"]').val(wl.doff_date);
                            $('input[name="floor_code"]').val(wl.floor_code);
                        }else if(wl.packed_status != 1 && wl.active_status == 1){
                            alert('Spindel is not Added to Challan or Alradey Repacked');
                        }else{
                            alert('Call Admin...');
                        }

                    });
                }else{
                    alert('No data found for this QR Code');

                }
            }
            $('.print').on('click',function(e){
                e.preventDefault();
                $id = $(this).data("id");
                $.ajax({
                    type: "GET",
                    url: "/print-weight-log-qr/"+$id,
                    success: function(data) {
                        alert('Sticker Printed');
                    },
                    error: function(xhr) {
                        console.log(xhr);
                    },
                });
            });
        });

    </script>

</body>
</html>
