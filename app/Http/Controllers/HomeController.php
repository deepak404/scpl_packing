<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $connected = @fsockopen("erp.shakticords.com", 80);
        if ($connected) {
            //action when connected
            fclose($connected);
        } else {
            //action in connection failure
            abort(503, 'No Internet');
        }
    }
    


    public function index()
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('GET', 'http://erp.shakticords.com/api/get-packing-case-no');

        $count = json_decode($response->getBody()->getContents());

        $lastEntry = $count->lastEntry;
        if (!empty($lastEntry->packed_data)) {
            $lastEntry->packed_date = date('d-m-Y', strtotime($lastEntry->packed_date));
        }

        $packingMaster = $count->packingMaster;

        $packingResult = [];


        foreach ($packingMaster as $name => $packings) {
            foreach ($packings as $packing) {
                if ($packing->parameter == 'Dimension') {
                    $packingResult[$name]['name'] = $packing->name;
                    $packingResult[$name]['dimension'] = $packing->value;
                }

                if ($packing->parameter == 'Weight') {
                    $packingResult[$name]['value'] = explode('±', $packing->value)[0];
                }
            }
        }


        $manufacturedDate = $count->manufactured_date;

        return view('home')->with(compact('count', 'lastEntry', 'packingResult', 'manufacturedDate'));
    }


    public function ncrPacking()
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('GET', 'http://erp.shakticords.com/api/get-packing-case-no');

        $count = json_decode($response->getBody()->getContents());
        $lastEntry = $count->lastEntry;
        $lastEntry->packed_date = date('d-m-Y', strtotime($lastEntry->packed_date));

        $packingMaster = $count->packingMaster;
        $packingResult = [];


        foreach ($packingMaster as $name => $packings) {
            foreach ($packings as $packing) {
                if ($packing->parameter == 'Dimension') {
                    $packingResult[$name]['name'] = $packing->name;
                    $packingResult[$name]['dimension'] = $packing->value;
                }

                if ($packing->parameter == 'Weight') {
                    $packingResult[$name]['value'] = explode('±', $packing->value)[0];
                }
            }
        }
      
        $manufacturedDate = $count->manufactured_date;

        return view('ncr-packing')->with(compact('count', 'lastEntry', 'packingResult', 'manufacturedDate'));
    }

    public function leaderPacking()
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('GET', 'http://erp.shakticords.com/api/get-packing-case-no');

        $count = json_decode($response->getBody()->getContents());


        $lastEntry = $count->lastEntry;
        $lastEntry->packed_date = date('d-m-Y', strtotime($lastEntry->packed_date));

        $packingMaster = $count->packingMaster;
        $packingResult = [];


        foreach ($packingMaster as $name => $packings) {
            foreach ($packings as $packing) {
                if ($packing->parameter == 'Dimension') {
                    $packingResult[$name]['name'] = $packing->name;
                    $packingResult[$name]['dimension'] = $packing->value;
                }

                if ($packing->parameter == 'Weight') {
                    $packingResult[$name]['value'] = explode('±', $packing->value)[0];
                }
            }
        }
        // dd($packingResult);
        $material = $count->material;
        $packedEntry = $count->packedEntry;
        $filament = $count->filament;
        // dd($lastEntry, $packedEntry);
        return view('leader-packing')->with(compact('count', 'lastEntry', 'packingResult', 'material', 'packedEntry', 'filament'));
    }

    public function getPackedCases($page=null)
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $url = 'http://erp.shakticords.com/api/v2/get-packed-cases?page='.$page ?? '1';
        $response = $client->request('GET', $url);

        $response = json_decode($response->getBody()->getContents());
        // dd($response);
        // $weightLogs = (array)$response->weightLogs;
        $packages = $response->data;
        $currentPage = $response->current_page;
        $lastPage = $response->last_page;
        $pageArray = [];
        $start = $currentPage - 2;
        $end = $currentPage + 3;
        if ($start < 1) {
            $start = 1;
            $end = 6;
        }
        for ($i=$start; $i < $end; $i++) {
            if ($lastPage < $i) {
                break;
            }
            $pageArray[] = $i;
        }


//         //This would contain all data to be sent to the view
//         $data = array();

//         //Get current page form url e.g. &page=6
//         $currentPage = LengthAwarePaginator::resolveCurrentPage();

//         //Create a new Laravel collection from the array data
//         $collection = new Collection($packages);

//         //Define how many items we want to be visible in each page
//         $per_page = 25;

//         //Slice the collection to get the items to display in current page
//         $currentPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();

//         //Create our paginator and add it to the data array
//         $data['results'] = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);

//         //Set base url for pagination links to follow e.g custom/url?page=6
//         $data['results']->setPath($request->url());

//         //Pass data to view
        // //        return view('search', $data);

//         $packages = $data['results'];

//        dd($packages['results']);
        return view('packed-cases')->with(compact('packages', 'currentPage', 'lastPage', 'pageArray'));
    }


    public function editPackage(Request $request)
    {
        $request->validate([
            'reason' => 'required',
            'package-id' => 'required'
        ]);


        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/edit-package', [
            'form_params' => [
                'package_id' => $request['package-id'],
                'reason' => $request['reason'],
            ]
        ]);


        return response()->json(['response' => json_decode($response->getBody()->getContents())]);
    }

    public function checkPassword(Request $request)
    {
        if ($request['password'] == 'supervisor!23') {
            return response()->json(['message' => true]);
        }

        return response()->json(['message' => false]);
    }

    public function printQrCode($data)
    {
        $date = date('d.m.Y', strtotime($data->doff_date));

        $text = "'Seagull:2.1:DP
                INPUT OFF
                VERBOFF
                INPUT ON
                SYSVAR(48) = 0
                ERROR 15,\"FONT NOT FOUND\"
                ERROR 18,\"DISK FULL\"
                ERROR 26,\"PARAMETER TOO LARGE\"
                ERROR 27,\"PARAMETER TOO SMALL\"
                ERROR 37,\"CUTTER DEVICE NOT FOUND\"
                ERROR 1003,\"FIELD OUT OF LABEL\"
                SYSVAR(35)=0
                OPEN \"tmp:setup.sys\" FOR OUTPUT AS #1
                PRINT#1,\"Printing,Media,Print Area,Media Margin (X),0\"
                PRINT#1,\"Printing,Media,Clip Default,On\"
                CLOSE #1
                SETUP \"tmp:setup.sys\"
                KILL \"tmp:setup.sys\"
                CLL
                OPTIMIZE \"BATCH\" ON
                PP25,183:AN7
                BARSET \"QRCODE\",1,1,5,2,1
                PB \"$data->unique_id\"
                PP155,188:NASC 8
                FT \"Univers Bold\"
                FONTSIZE 8
                FONTSLANT 0
                PT \"$data->material\"
                PP155,158:PT \"DOFF : $data->doff_no\"
                PP155,129:PT \"DOP : $date\"
                PP155,98:PT \"SPINDLE NO : $data->spindle\"
                PP21,64:FONTSIZE 7
                PT \"$data->floor_code\"
                LAYOUT RUN \"\"
                PF
                PRINT KEY OFF";



        file_put_contents('output.prn', $text);

        shell_exec('COPY ' .public_path(). '\output.prn /B \\\127.0.0.1\honeywell2');
    }

    /**
     * Repack get
     *
     * @return void
     */
    public function repack()
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('GET', 'http://erp.shakticords.com/api/get-repacking');

        $count = json_decode($response->getBody()->getContents());
        $material = $count->material;
        $packedEntry = $count->packedEntry;
        // dd($material);
        return view('repack')->with(compact('material', 'packedEntry'));
    }

    /**
     * Create Repack
     *
     * @param Request $request
     * @return void
     */
    public function createRepack(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'doff_no' => 'required',
            'doff_date' => 'required|date',
            'material' => 'required',
            'floor_code' => 'required',
            'spindle' => 'required'
        ]);


        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/create-repack', [
            'form_params' => [
                'id' => $request['id'],
                'doff_no' => $request['doff_no'],
                'doff_date' => date('Y-m-d 07:00:00', strtotime($request['doff_date'])),
                'material' => $request['material'],
                'floor_code' => $request['floor_code'],
                'spindle' => $request['spindle'],
            ]
        ]);

        $result = json_decode($response->getBody()->getContents());
        
        if (empty($result->status)) {
            return redirect()->back()->with(['error'=>'Unable to connect with ERP System.']);
        } else {
            if ($result->status) {
                $this->printQrCode($result->data);
                $this->printQrCode($result->data);
                return redirect('/re-packing');
            } else {
                return redirect()->back()->with(['error'=>$result->response]);
            }
        }
    }

    /**
     * Print Weight Log
     *
     * @param [type] $id
     * @return void
     */
    public function printWeightLogQr($id)
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $url = 'http://erp.shakticords.com/api/get-weight-log/'.$id;
        $response = $client->request('GET', $url);

        $result = json_decode($response->getBody()->getContents());

        $this->printQrCode($result->data);

        return response()->json(['status'=>true]);
    }

    public function braiding()
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('GET', 'http://erp.shakticords.com/api/get-braiding-pack');

        $count = json_decode($response->getBody()->getContents());
        $lastEntry = $count->lastEntry;
        if (!empty($lastEntry->packed_data)) {
            $lastEntry->packed_date = date('d-m-Y', strtotime($lastEntry->packed_date));
        }

        $packingMaster = $count->packingMaster;

        $packingResult = [];


        foreach ($packingMaster as $name => $packings) {
            foreach ($packings as $packing) {
                if ($packing->parameter == 'Dimension') {
                    $packingResult[$name]['name'] = $packing->name;
                    $packingResult[$name]['dimension'] = $packing->value;
                }

                if ($packing->parameter == 'Weight') {
                    $packingResult[$name]['value'] = explode('±', $packing->value)[0];
                }
            }
        }


        $manufacturedDate = $count->manufactured_date;

        return view('braiding-packing')->with(compact('count', 'lastEntry', 'packingResult', 'manufacturedDate'));
    }

    public function braidingNcr()
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('GET', 'http://erp.shakticords.com/api/get-braiding-pack');

        $count = json_decode($response->getBody()->getContents());
        $lastEntry = $count->lastEntry;
        if (!empty($lastEntry->packed_data)) {
            $lastEntry->packed_date = date('d-m-Y', strtotime($lastEntry->packed_date));
        }

        $packingMaster = $count->packingMaster;

        $packingResult = [];


        foreach ($packingMaster as $name => $packings) {
            foreach ($packings as $packing) {
                if ($packing->parameter == 'Dimension') {
                    $packingResult[$name]['name'] = $packing->name;
                    $packingResult[$name]['dimension'] = $packing->value;
                }

                if ($packing->parameter == 'Weight') {
                    $packingResult[$name]['value'] = explode('±', $packing->value)[0];
                }
            }
        }


        $manufacturedDate = $count->manufactured_date;

        return view('braiding-ncr-packing')->with(compact('count', 'lastEntry', 'packingResult', 'manufacturedDate'));
    }

    public function createBraiding(Request $request)
    {
        $request->validate([
            'doff_no' => 'required',
            'material_id' => 'required',
            'filament' => 'required',
            'b_count' => 'required',
            'box-type' => 'required',
            'case-no' => 'required',
            'moisture-regain' => 'required',
            'box-weight' => 'required',
            'package-date' => 'required',
            'inset-weight' => 'required',
            'dispatch-date' => 'required',
            'package-type' => 'required',
            'g_wt' => 'required',
        ]);
        try {
            $processData = $request->all();
            unset($processData['_token']);

            $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
            $response = $client->request('POST', 'http://erp.shakticords.com/api/create-braiding', [
                'form_params' => $processData
            ]);

            $result = json_decode($response->getBody()->getContents());
            
            if ($result->status) {
                return redirect()->back();
            } else {
                return redirect()->back()->withErrors([$result->response]);
            }
        } catch (\Exception $th) {
            return redirect()->back()->withErrors([$th->getMessage()]);
        }
    }
}
