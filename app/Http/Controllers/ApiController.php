<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    public function getBarcodeDetails(Request $request)
    {

//        return $request['data'];

        $apiKey = $request['api-key'];



        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/get-barcode-weight-log', [
            'form_params' => [
                'unique_id' => $request['data'],
                'api_key' => 'scpl-inspection-003',
            ]
        ]);


//        foreach((array)json_decode($response->getBody()->getContents()) as $key => $weightLog){
//
//            if($weightLog->active_status == 1 && $weightLog->inspection_status == 1){
//
//            }else{
//                unset($weightLog[$key]);
//            }
//        }


//
//        \Log::info($response->getBody()->getContents());
        ////        return "hello";
        ////        die();


        $data = [
            'event' => $request['event'],
            'data' =>  $response->getBody()->getContents(),
        ];

//        return $data;

        Redis::publish('barcode-channel', json_encode($data));
    }
    
    public function sendWeightLog(Request $request)
    {
        $data = [
            'event' => $request['event'],
            'data' => $request['data']
        ];

        Redis::publish('weightlog-channel', json_encode($data));
    }

    public function addPackageDetails(Request $request)
    {
        $request->validate([
            'box_type' => 'required',
            'case_no' => 'required',
            'package_type' => 'required',
            'package_date' => 'required',
            'bobbin_count' => 'required',
            'weight_log' => 'required',
            'moisture_regain' => 'required',
            'box_weight' => 'required',
            'packing_box' => 'required',
            'material_type' => 'required',
//            'inset_weight' => 'required',
//            'dispatch_date' => 'required',
//            'expiry_date' => 'required',
        ]);

        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);

        if ($request['expiry_date'] != null) {
            $request['expiry_date'] = date('Y-m-d', strtotime($request['expiry_date']));
        }

        $response = $client->request('POST', 'http://erp.shakticords.com/api/add-packaging-details', [
            'form_params' => [
                'box_type' => $request['box_type'],
                'case_no' => $request['case_no'],
                'package_type' => $request['package_type'],
                'package_date' => $request['package_date'],
                'bobbin_count' => $request['bobbin_count'],
                'weight_log' => $request['weight_log'],
                'reason' => $request['info'],
                'moisture_regain' => $request['moisture_regain'],
                'box_weight' => $request['box_weight'],
                'packing_box' => $request['packing_box'],
                'material_type' => $request['material_type'],
                'dispatch_date' => $request['dispatch_date'],
                'expiry_date' => $request['expiry_date'],
                'inset_weight' => $request['inset_weight'] ?? 0,
            ]
        ]);

        $response = $response->getBody()->getContents();
//        return response()->json(['data' => $response]);


        if (json_decode($response)->status) {
            $id = json_decode($response)->id;
            $this->printPackageQr($id);
        }

        return response()->json(['data' => $response]);
    }

    public function leaderIndirectPacking(Request $request)
    {
        $request->validate([
            'box-type' => 'required',
            'case-no' => 'required',
            'package-type' => 'required',
            'package-date' => 'required',
            'bobbin-count' => 'required',
            'scale_weight' => 'required',
            'box-weight' => 'required',
            'packing-box' => 'required',
            'material_type' => 'required',
        ]);
        // dd($request->all());

        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/add-leader-indirect-packing', [
            'form_params' => [
                'box_type' => $request['box-type'],
                'case_no' => $request['case-no'],
                'package_type' => $request['package-type'],
                'package_date' => $request['package-date'],
                'bobbin_count' => $request['bobbin-count'],
                'scale_weight' => $request['scale_weight'],
                'reason' => $request['info'] ?? null,
                'moisture_regain' => 0,
                'box_weight' => $request['box-weight'],
                'packing_box' => $request['packing-box'],
                'material_type' => $request['material_type'],
                'material_id' => $request['material'],
                'filament' => null,
                'tare_weight' => $request['tare_weight'],
            ]
        ]);

        $result = $response->getBody()->getContents();
        $result = json_decode($result);

        return redirect('/leader-packing');
    }

    public function printPackQr(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $this->printPackageQr($request['id']);
    }


    public function generateManualStickers(Request $request)
    {
        $request->validate([
            'from-case-no' => 'required',
            'to-case-no' => 'required',
        ]);


        $caseNo = [];


        for ($i= $request['from-case-no']; $i<=$request['to-case-no']; $i++) {
            $caseNo[] = (int)$i;
        }

        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/get-manual-cases', [
            'form_params' => [
                'unique_id' => '1234',
                'case_no' => $caseNo,
            ]
        ]);

        $response = json_decode($response->getBody()->getContents());

        foreach ($response as $packageId) {
            $this->printPackageQr($packageId);
//            sleep(1);
        }
    }

    private function printPackageQr($id)
    {
        $client = new Client(['headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']]);
        $response = $client->request('POST', 'http://erp.shakticords.com/api/get-qr-package', [
            'form_params' => [
                'package_id' => $id,
            ]
        ]);



        $response = json_decode($response->getBody()->getContents());
        $package = (array)$response->package;
        $weightLogs = (array)$response->weightLogs;




        $doff_no = implode(',', array_unique(array_column($weightLogs, 'doff_no')));


        $manufacturingDate = date('d-m-Y', strtotime(implode(',', array_unique(array_column($weightLogs, 'doff_date')))));

        $grossWeight = round((array_sum(array_column($weightLogs, 'total_weight')) + $package['inset_weight'] + $package['box_weight']), 1);

        $netWeight = round(array_sum(array_column($weightLogs, 'material_weight')) + $package['moisture_weight'], 1);

        $bobbinCount = $package['bobbin_count'];
        $caseNo = $package['case_no'];
        $packageId = $package['id'];

        if ($package['expiry_date'] != null) {
            $expiryDate = date('d-m-Y', strtotime($package['expiry_date']));
        } else {
            $expiryDate = null;
        }
        $dispatchDate = $package['dispatch_date'];

        $materialString = explode(' ', $weightLogs[0]->packing_name);
        $packingName = $weightLogs[0]->packing_name;

        if ($package['package_type'] == 'BOBBIN') {
            $packageString = "NO OF BOBBINS : ".$bobbinCount;
        } else {
            $packageString = "NO OF CHEESES : ".$bobbinCount;
        }
        $materialString1 = implode(' ', array_splice($materialString, 0, -3));
        $materialString2 = implode(' ', array_slice($materialString, -3, 3, true));

//        return [$materialString1, $materialString2];


        $text1 = "'Seagull:2.1:DP
                    INPUT OFF
                    VERBOFF
                    INPUT ON
                    SYSVAR(48) = 0
                    ERROR 15,\"FONT NOT FOUND\"
                    ERROR 18,\"DISK FULL\"
                    ERROR 26,\"PARAMETER TOO LARGE\"
                    ERROR 27,\"PARAMETER TOO SMALL\"
                    ERROR 37,\"CUTTER DEVICE NOT FOUND\"
                    ERROR 1003,\"FIELD OUT OF LABEL\"
                    SYSVAR(35)=0
                    OPEN \"tmp:setup.sys\" FOR OUTPUT AS #1
                    PRINT#1,\"Printing,Media,Print Area,Media Margin (X),0\"
                    PRINT#1,\"Printing,Media,Clip Default,On\"
                    CLOSE #1
                    SETUP \"tmp:setup.sys\"
                    KILL \"tmp:setup.sys\"
                    CLL
                    OPTIMIZE \"BATCH\" ON
                    PP288,981:AN7
                    BARSET \"QRCODE\",1,1,6,2,1
                    PB \"$packingName | BOX NO : $caseNo | DOFF NO : $doff_no  |  DOM : $manufacturingDate | DOE : $expiryDate\"
                    PP80,714:NASC 8
                    FT \"Univers Condensed Bold\"
                    FONTSIZE 14
                    FONTSLANT 0
                    PT \"CODE : $materialString1\"
                    PP80,669:PT \"$materialString2\"
                    PP80,607:PT \"DOFF NO : $doff_no\"
                    PP80,242:PT \"MANUFACTURING DATE : $manufacturingDate\"
                    PP80,464:PT \"GROSS WEIGHT : $grossWeight Kgs\"
                    PP80,391:PT \"NET WEIGHT : $netWeight Kgs\"
                    PP80,319:PT \"$packageString\"
                    PP80,535:PT \"BOX NO : $caseNo\"
                    ";

        if ($dispatchDate == 'yes' && $expiryDate != null) {
            $text1 = $text1."PP80,101:PT \"EXPIRY DATE : $expiryDate\"
                            PP80,171:PT \"DISPATCH DATE : \"
                            ";
        } elseif ($dispatchDate == 'no' && $expiryDate == null) {
        } elseif ($dispatchDate == 'yes' && $expiryDate == null) {
            $text1 = $text1."PP80,171:PT \"DISPATCH DATE : \"";
        } else {
            $text1 = $text1."PP80,171:PT \"EXPIRY DATE : $expiryDate\"";
        }


        $text1 = $text1."
                    PP244,1182:FONTSIZE 10
                    PT \"FOR INDUSTRIAL USE ONLY\"
                    PP200,1145:FONTSIZE 14
                    PT \"SHAKTI CORDS PVT. LTD\"
                    PP97,1100:FONTSIZE 10
                    PT \"CS - 17&18, SIDCO INDUSTRIAL ESTATE, KAPPALUR\"
                    PP293,1069:PT \"MADURAI - 625008\"
                    LAYOUT RUN \"\"
                    PF
                    PRINT KEY OFF";


//        $text = "'Seagull:2.1:DP
//                INPUT OFF
//                VERBOFF
//                INPUT ON
//                SYSVAR(48) = 0
//                ERROR 15,\"FONT NOT FOUND\"
//                ERROR 18,\"DISK FULL\"
//                ERROR 26,\"PARAMETER TOO LARGE\"
//                ERROR 27,\"PARAMETER TOO SMALL\"
//                ERROR 37,\"CUTTER DEVICE NOT FOUND\"
//                ERROR 1003,\"FIELD OUT OF LABEL\"
//                SYSVAR(35)=0
//                OPEN \"tmp:setup.sys\" FOR OUTPUT AS #1
//                PRINT#1,\"Printing,Media,Print Area,Media Margin (X),0\"
//                PRINT#1,\"Printing,Media,Print Area,Media Width,799\"
//                PRINT#1,\"Printing,Media,Print Area,Media Length,1200\"
//                PRINT#1,\"Printing,Print Quality,Darkness,100\"
//                PRINT#1,\"Printing,Media,Clip Default,On\"
//                CLOSE #1
//                SETUP \"tmp:setup.sys\"
//                KILL \"tmp:setup.sys\"
//                CLL
//                OPTIMIZE \"BATCH\" ON
//                PP190,1142:AN7
//                BARSET \"QRCODE\",1,1,20,2,1
//                PB \"$packageId\"
//                PP31,605:NASC 8
//                FT \"Univers Bold\",20,0,50
//                PT \"DOFF NO : $doff_no\"
//                PP31,538:FT \"Univers Bold\",20,0,50
//                PT \"MANUFACTURING DATE : $manufacturingDate\"
//                PP31,456:FT \"Univers Bold\",20,0,50
//                PT \"GROSS WEIGHT : $grossWeight KGS\"
//                PP31,392:FT \"Univers Bold\",20,0,50
//                PT \"NET WEIGHT : $netWeight KGS\"
//                PP31,326:FT \"Univers Bold\",20,0,50
//                PT \"NO OF BOBBINS / CHEESE : $bobbinCount\"
//                PP31,259:FT \"Univers Bold\",20,0,50
//                PT \"BOX NO : $caseNo\"
//                ";
//
//
//        if($dispatchDate == 'yes' && $expiryDate != null){
//            $text =  $text."PP31,193:FT \"Univers Bold\",20,0,50
//                PT \"BEST BEFORE : $expiryDate\"
//                PP31,126:FT \"Univers Bold\",20,0,50
//                PT \"DISPATCH DATE : \"
//                PP31,669:FT \"Univers Bold\",20,0,50
//                PT \"CODE : $response->material_name\"
//                LAYOUT RUN \"\"
//                PF
//                PRINT KEY OFF";
//        }else if($dispatchDate == 'no' && $expiryDate == null){
//            $text =  $text."PP31,669:FT \"Univers Bold\",20,0,50
//                PT \"CODE : $response->material_name\"
//                LAYOUT RUN \"\"
//                PF
//                PRINT KEY OFF";
//        }else if($dispatchDate == 'yes' && $expiryDate == null){
//            $text = $text."PP31,126:FT \"Univers Bold\",20,0,50
//                PT \"DISPATCH DATE : \"
//                PP31,669:FT \"Univers Bold\",20,0,50
//                PT \"CODE : $response->material_name\"
//                LAYOUT RUN \"\"
//                PF
//                PRINT KEY OFF";
//        }else{
//            $text =  $text."PP31,193:FT \"Univers Bold\",20,0,50
//                PT \"BEST BEFORE : $expiryDate\"
//                PP31,669:FT \"Univers Bold\",20,0,50
//                PT \"CODE : $response->material_name\"
//                LAYOUT RUN \"\"
//                PF
//                PRINT KEY OFF";
//        }

        file_put_contents('output.prn', $text1);

        shell_exec('COPY ' .public_path(). '\output.prn /B \\\127.0.0.1\honeywell');
    }
}
