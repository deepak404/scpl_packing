import serial
import requests
import re
import time

#weightlog machine Config
API_KEY1 = 'dummy'
API_ENDPOINT1 = 'http://127.0.0.1:8000/api/send-weight-log'
ser1 = serial.Serial()
ser1.port = 'COM4'
ser1.baudrate=9600
ser1.timeout=1
ser1.parity=serial.PARITY_ODD
ser1.stopbits=serial.STOPBITS_TWO
ser1.bytesize=serial.SEVENBITS
ser1.open()
print("connected to Weightlog Machine: " + ser1.portstr)


API_KEY2 = 'dummy'
API_ENDPOINT2 = 'http://127.0.0.1:8000/api/get-barcode'

ser2 = serial.Serial()
ser2.port = 'COM3'
ser2.baudrate=9600
ser2.timeout=1
ser2.parity=serial.PARITY_ODD
ser2.stopbits=serial.STOPBITS_TWO
ser2.bytesize=serial.SEVENBITS
ser2.open()
print("connected to QR Scanner: " + ser2.portstr)



while True:
    #Below is to read the Weightlog machine.
    bytesToRead1 = ser1.inWaiting()
    data1 = re.findall("\d+\.\d+", ser1.read(bytesToRead1).decode('utf-8'))
    if data1:
        weight = max(data1)
        print(weight)
        POST_DATA1 = {'data':weight,'event':'weightLogEvent'}
        response = requests.post(url = API_ENDPOINT1, data = POST_DATA1)
        print('Response from API : '+response.text)
    time.sleep(1)

    #below is to read the QR Scanning Machine
    bytesToRead2 = ser2.inWaiting()
    data2 = ser2.read(ser2.inWaiting()).decode('utf-8')

    
    if data2:
        POST_DATA2 = {
                     'event': 'barcodeEvent' ,
                     'data': data2,
                     }
        response = requests.post(url = API_ENDPOINT2, data = POST_DATA2) 
        print('Barcode Updated'+ data2)

