<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/ncr-packing', 'HomeController@ncrPacking');

Route::post('/add-package-details', 'ApiController@addPackageDetails');


Route::get('/leader-packing', 'HomeController@leaderPacking');

Route::post('/add-leader-indirect-packing', 'ApiController@leaderIndirectPacking');

Route::get('/get-packed-cases/{page?}', 'HomeController@getPackedCases');

Route::get('/print-weight-log-qr/{id}', 'HomeController@printWeightLogQr');

Route::get('/print-sticker-range', function () {
    return view('manual-sticker');
});

Route::get('/re-packing', 'HomeController@repack');

Route::post('/create-repack', 'HomeController@createRepack');

Route::post('/generate-manual-stickers', 'ApiController@generateManualStickers');

Route::post('/print-pack-qr', 'ApiController@printPackQr');

Route::post('/edit-package', 'HomeController@editPackage');

Route::post('/check-password', 'HomeController@checkPassword');

Route::get('/testPrint', 'HomeController@printQrCode');

Route::get('/braiding', 'HomeController@braiding');

Route::get('/braiding-ncr', 'HomeController@braidingNcr');

Route::post('/create-braiding', 'HomeController@createBraiding');
